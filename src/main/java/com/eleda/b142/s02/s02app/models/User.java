package com.eleda.b142.s02.s02app.models;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User {
    // Properties (columns)
    @Id
    @GeneratedValue
    private Long id; // primary key
    @Column
    private String username;
    @Column
    private String password;

    // Constructors
    // Data: title, content

    // Empty
    public User() {}

    // Parameterized
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // Getters & Setters
    //Getters
    public String getUsername() {

        return username;
    }

    public String getPassword() {

        return password;
    }

    //Setters
    public void setUsername(String newUsername) {

        this.username = newUsername;
    }

    public void setPassworrd(String newPassword) {

        this.password = newPassword;
    }
    // Methods
}
